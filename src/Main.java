import java.io.IOException;
import java.util.*;

import javax.xml.parsers.ParserConfigurationException;

import org.json.simple.parser.ParseException;
import org.xml.sax.SAXException;

import admin.Librarian;
import admin.LibrarianMember;
import book.*;
import users.student.*;
import users.staff.*;
import Helper.Util.Input;



class Main{    
    public static LibrarianMember lib = new LibrarianMember();
    public static Integer userType;
    public static String userName, password;
    public static StudentMember stu = new StudentMember();
    public static StudentMember.CurrentStudent curStu = stu.new CurrentStudent(); 
    static StaffMember stf = new StaffMember();
    public static StaffMember.CurrentStaff curStaff = stf.new CurrentStaff(); 

    public static boolean login(){
        String userName, password;
        userName = Input.input("Enter UserName : ");
        password = Input.input("Enter Password : ");
        if(userType == 1){
            List<Student> students = StudentMember.getStudents();
            for(int ind = 0; ind < students.size(); ind++){
                Student stu = students.get(ind);
                if(stu.userName.equals(userName) && stu.password.equals(password)){
                    curStu.setStudentData(userName, stu.id);
                  
                    System.out.println("Successfully Loged in...");
                    return true;
                }
            }
        }
        else if(userType == 2){
            List<Staff> staffs = StaffMember.getStaffs();
            for(int ind = 0; ind < staffs.size(); ind++){
                Staff stf = staffs.get(ind);
                if(stf.userName.equals(userName) && stf.password.equals(password)){
                    curStaff.setStaffData(userName, stf.id);
                    System.out.println("Successfully Loged in...");
                    return true;
                }
            }
        }
        else if(userType == 3){
            List<Librarian> librarians = lib.getLibrarians();
            for(int ind = 0; ind < librarians.size(); ind++){
                Librarian li = librarians.get(ind);
                if(li.name.equals(userName) && li.password.equals(password)){
                    lib.setLibData(userName, li.id);
                    System.out.println("Successfully Loged in...");
                    return true;
                }
            }
        }
        System.out.println("Invalid Username or Password...");
        return false;
    }

    public static void signin(){
        String usertype = Input.input("* Enter UserType : ");
        if (usertype.toLowerCase().equals("student")){
            String userName, password, email, department;
            userName = Input.input("Enter UserName : ");
            password = Input.input("Enter Password : ");
            email = Input.input("Enter Email : ");
            department = Input.input("Enter Department : ");

            try{
                StudentMember.addStudent(StudentMember.getStudents().size(),userName, password, email, department);
                System.out.println("Account created...");
            }
            catch(Exception e){
                e.printStackTrace();
            }

        }else if(usertype.toLowerCase().equals("staff")){
            String userName, password, department, email, subject;
            userName = Input.input("Enter UserName : ");
            password = Input.input("Enter Password : ");
            department = Input.input("Enter Department : ");
            email = Input.input("Enter Email : ");
            subject = Input.input("Enter Subject : ");

            try{
                Integer id = StaffMember.getStaffs().size();
                StaffMember.addStaff(Integer.toString(id), userName, password, email, department, subject);
                System.out.println("Account created...");
            }
            catch(Exception e){
                e.printStackTrace();
            }
        }else if(usertype.toLowerCase().equals("librarian")){
            String userName, password, email;
            userName = Input.input("Enter UserName : ");
            password = Input.input("Enter Password : ");
            email = Input.input("Enter Email : ");

            try{
                lib.addLibrarian(lib.getLibrarians().size(), userName, password, email);
                System.out.println("Account created...");
            }
            catch(Exception e){
                e.printStackTrace();
            }
        }else{
            System.out.println("Invalid UserType...");
            signin();
        }
    }

    public static void logout(){
        if(userType == 1){
            curStu.setStudentData(null, -1);
        }
        else if(userType == 2){
            curStaff.setStaffData(null, -1);
        }
        else if(userType == 3){
            lib.setLibData(null, -1);
        }
        System.out.println("Successfully Loged Out...");
    }

    @SuppressWarnings("unused")
    public static void main(String[] arg) throws IOException, ArrayIndexOutOfBoundsException, ParseException, ParserConfigurationException, SAXException {
        
        stu.readStu();
        stf.readStaff();
        lib.readLib();
        lib.readBook();

        while(true){
            userType = Input.Int(Input.input("* Enter 1 for Student \n* Enter 2 for Staff \n* Enter 3 for Librarian\n* Enter 4 for SignIn\n* Enter 5 for Exist\nEnter Number : "));
            if(userType == 1){
                if(login()){
                    while(true){
                        while(curStu.getStuId() != -1){
                            Integer num = Input.Int(Input.input("* Enter 0 for Renew Book\n* Enter 1 for View Book\n* Enter 2 for Search Book\n* Enter 3 for Check Book Status\n* Enter 4 for Logout\nPress 1 - 4 number : "));
                            if(num == 0){
                                String bookId = Input.input("Enter Book Isbn Number : ");
                                curStu.renewBook(bookId);
                            }else if(num == 1){
                                lib.showBook();
                            }else if(num == 2){
                                String bookName = Input.input("Enter Book Name : ");
                                Integer bookInd = lib.searchBook(bookName);
                                if (bookInd != -1){
                                Book book = BookStore.books.get(bookInd);
                                System.out.println(String.format(String.format("\nBook :\nIsbn : %o\nBookName : %s\nAuthor : %s\nYear : %o\nBookCopy : %o\n",book.isbn,book.name, book.author, book.year, book.bookCopy)));
                                } 
                                else System.out.println("Book Not Found...");
                            }else if(num == 3){
                                curStu.generateBorrowedBook();
                            }else if(num == 4){
                                logout();
                            }else{
                                System.out.println("Please Enter Number Between 1 - 5..\n");
                            }
                        }break;
                    }
                }
            }
            else if(userType == 2){
                if(login()){
               
                    while(true){
                        while(curStaff.getStaffId() != -1){
                            Integer num = Input.Int(Input.input("* Enter 0 for Renew Book\n* Enter 1 for View Book\n* Enter 2 for Search Book\n* Enter 3 for Check Book Status\n* Enter 4 for Logout\nPress 1 - 4 number : "));
                            if(num == 0){
                                String bookId = Input.input("Enter Book Isbn Number : ");
                                curStaff.renewBook(bookId);
                            }else if(num == 1){
                                lib.showBook();
                            }else if(num == 2){
                                String bookName = Input.input("Enter Book Name : ");
                                Integer bookInd = lib.searchBook(bookName);
                                if (bookInd != -1){
                                Book book = BookStore.books.get(bookInd);
                                System.out.println(String.format(String.format("\nBook :\nIsbn : %o\nBookName : %s\nAuthor : %s\nYear : %o\nBookCopy : %o\n",book.isbn,book.name, book.author, book.year, book.bookCopy)));
                                } 
                                else System.out.println("Book Not Found...");
                            }else if(num == 3){
                                curStaff.generateBorrowedBook();
                            }else if(num == 4){
                                logout();
                            }else{
                                System.out.println("Please Enter Number Between 1 - 5..\n");
                            }
                        }break;
                    }
                }
            }
            else if(userType == 3){
                if(login()){
                    while(true){
                        Integer num = Input.Int(Input.input("* Enter 0 for Issue Book\n* Enter 1 for Search Book\n* Enter 2 for Add Book\n* Enter 3 for Delete Book\n* Enter 4 for View Books\n* Enter 5 for Return Book\n* Enter 6 for Generate BorrowedBook\n* Enter 7 for Logout \nPress 1 - 7 number : "));
                        if(num == 0){
                            lib.issueBook();
                        }
                        else if(num == 1){
                            String bookName = Input.input("Enter Book Name : ");
                            Integer bookInd = lib.searchBook(bookName);
                            if (bookInd != -1){
                               Book book = BookStore.books.get(bookInd);
                               System.out.println(String.format(String.format("\nBook :\nIsbn : %o\nBookName : %s\nAuthor : %s\nYear : %o\nBookCopy : %o\n",book.isbn,book.name, book.author, book.year, book.bookCopy)));
                            } 
                            else System.out.println("Book Not Found...");
                        }
                        else if(num == 2){
                            Integer isbn;
                            String name, author, year, bookCopy;
                            isbn = Input.Int(Input.input("Enter Isbn Number : "));
                            name = Input.input("Enter Book Name : ");
                            author = Input.input("Enter Author Name : ");
                            year = Input.input("Enter published Year : ");
                            bookCopy = Input.input("Enter bookCopy : ");
                            lib.addBook(isbn, name, author, year, bookCopy);
                            System.out.println("Book created...");
                        }
                        else if(num == 3){
                            Integer isbn = Input.Int(Input.input("Enter Isbn Number : "));
                            lib.deleteBook(isbn);
                        }
                        else if(num == 4){
                            lib.showBook();
                        }
                        else if(num == 5){
                            lib.returnBook();
                        }
                        else if(num == 6){
                            lib.generatedBorrowBook("student");
                            lib.generatedBorrowBook("staff");
                        }else if(num == 7){
                            logout();
                            break;
                        }else{
                            System.out.println("Please Enter Number Between 1 - 7");
                        }
                    }
                } 
             
            }
            else if(userType == 4){
                signin();
            }
            else if(userType == 5){
                break;
            }
            else{
                System.out.println("Enter Number Between 1 - 4\n");
            }
                       
            }       
        }
    }
