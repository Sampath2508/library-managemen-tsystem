package Helper.Util;
import java.util.Scanner;

public final class Input{
    public static Scanner inp = new Scanner(System.in);

    public final static String input(String display){
        System.out.print(display);
        return inp.nextLine();
    }

    public final static Integer Int(String val){
        return Integer.parseInt(val);
    }
}