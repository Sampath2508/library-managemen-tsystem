package book;

import Helper.Util.*;
import java.lang.ArrayIndexOutOfBoundsException;
import users.staff.StaffMember;
import users.student.StudentMember;

import java.io.*;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.util.*;

public class BookStore{
    
    public static List<Book> books = new ArrayList<Book>();

    public void readBook() throws ParseException, IOException, ArrayIndexOutOfBoundsException{
        JSONParser parser = new JSONParser();
        Object obj = parser.parse( new FileReader("C:\\Users\\inc1560\\Music\\LibraryManagementSystem\\LibraryManagementSystem\\files\\Book.json"));
        JSONObject booksJson = (JSONObject) obj;        
        JSONArray booksArr = (JSONArray) booksJson.get("books");

        for(int ind = 0; ind < booksArr.size(); ind++){
            JSONObject cur = (JSONObject) booksArr.get(ind);
            
            Integer isbn = Math.toIntExact((Long) cur.get("isbn"));
            Integer year = Math.toIntExact((Long) cur.get("year"));
            Integer bookCopy = Math.toIntExact((Long) cur.get("bookCopy"));

            this.books.add(new Book(isbn, (String) cur.get("name"), (String) cur.get("author"), year, bookCopy));
        }
    }

    public void addBook(Integer isbn, String name, String author, String year, String bookCopy) throws IOException{
        this.books.add(new Book(isbn, name, author, Input.Int(year), Input.Int(bookCopy)));
        JSONObject booksJson = new JSONObject();
        JSONArray arr = new JSONArray();
        for(Book book : books){
            JSONObject b = new JSONObject();
            b.put("isbn", book.isbn);
            b.put("name", book.name);
            b.put("author", book.author);
            b.put("year", book.year);
            b.put("bookCopy", book.bookCopy);
            arr.add(b);
        }
        booksJson.put("books", arr);
        FileWriter file = new FileWriter("C:\\Users\\inc1560\\Music\\LibraryManagementSystem\\LibraryManagementSystem\\files\\Book.json");
        file.write(booksJson.toJSONString());
        file.flush();
        file.close();
    }

    public void deleteBook(int isbn){
        for(int ind = 0; ind < books.size(); ind++){
            Book current = this.books.get(ind);
            if (current.isbn == isbn){
                this.books.remove(ind);
                System.out.println("Boook Deleted...\n");
                break;
            }else if (ind == books.size() - 1 && current.isbn != isbn){
                System.out.println("Boook Not Founded...\n");
            }
        }
    }

    public Integer searchBook(String bookName){
        for(int ind = 0; ind < books.size(); ind++){
            Book current = books.get(ind);
            if (current.name.contains(bookName)){
                return ind;
            }
        }
        return -1;
    }

    public Book getBookInfo(String bookname){
        Integer ind = searchBook(bookname);
        
        if (ind != -1) return this.books.get(ind);
        else return new Book();
    }   

    public void updateBookCopy(String bookName, Integer id){
        Integer ind = searchBook(bookName);
        Book current = this.books.get(ind);
        current.bookCopy -= current.bookCopy;
        this.books.set(ind, current);
    }

    public void issueBook(){
        String userType = Input.input("Student or Staff : ");
        int id = Input.Int(Input.input("Id : "));
        String bookName = Input.input("Book Name : ");

        int ind = searchBook(bookName);
        if (ind == -1) System.out.println("Book Not Founded...");
        else{
            
            Book book = this.books.get(ind);
            if(userType.toLowerCase().equals("student")){
                Map<Integer, HashMap<String, Integer>> borrowedBook = StudentMember.getBorrowedBook();
                HashMap<String, Integer> obj;
                if (borrowedBook.get(id) == null){
                    obj = new HashMap<String, Integer>();
                }else{
                    obj = borrowedBook.get(id);
                }
                obj.put(book.isbn+"-"+(book.bookCopy - book.bookId), 5);
                borrowedBook.put(id, obj);
                StudentMember.setBorrowedBook(borrowedBook);
            }else{
                Map<Integer, HashMap<String, Integer>> staffBorrowedBook = StaffMember.getBorrowedBook();
                HashMap<String, Integer> obj;
                if (staffBorrowedBook.get(id) == null){
                    obj = new HashMap<String, Integer>();
                }else{
                    obj = staffBorrowedBook.get(id);
                }
                obj.put(book.isbn+"-"+(book.bookCopy - book.bookId), 5);
                staffBorrowedBook.put(id, obj);
                StaffMember.setBorrowedBook(staffBorrowedBook);
            }            
            book.bookId = book.bookId - 1;
            this.books.set(ind, book);
            System.out.println("Book Issued...");
        }
    }

        public void showBook(){
            System.out.println("Books : ");
            for(Book book : books){
                System.out.println(String.format("Isbn : %o\nBookName : %s\nAuthor : %s\nYear : %o\nBookCopy : %o\n",book.isbn,book.name, book.author, book.year, book.bookCopy));
            }
        }

        public void generatedBorrowBook(String userType){
            if (userType == "student"){
                System.out.println("Students : ");
                Map<Integer, HashMap<String, Integer>> borrowedBook = StudentMember.getBorrowedBook();
                for(Integer stuId : borrowedBook.keySet()){
                    HashMap<String, Integer> bor = borrowedBook.get(stuId);
                    for(String key : bor.keySet()){
                        System.out.println(String.format("Student Id : %o\nBook Id : %s\nReturn Day : %o", stuId, key, bor.get(key)));
                    }
                }
            }
            
            else{
                System.out.println("Staffs : ");
                Map<Integer, HashMap<String, Integer>> staffBorrowedBook = StaffMember.getBorrowedBook();
                for(Integer staffId : staffBorrowedBook.keySet()){
                    HashMap<String, Integer> bor = staffBorrowedBook.get(staffId);
                    for(String key : bor.keySet()){
                        System.out.println(String.format("Staff Id : %o\nBook Id : %s\nReturn Day : %o", staffId, key, bor.get(key)));
                    }
                }
            }
        }
        
        public void returnBook(){
            String userType = Input.input("Enter Student or Staff : ");
            int id = Input.Int(Input.input("Enter Id : "));
            String bookName = Input.input("Book Name : ");

            int ind = searchBook(bookName);
            if (ind == -1) System.out.println("Book Not Founded...");
            else{
                Book book = this.books.get(ind);
                if(userType.toLowerCase() == "student"){
                    Map<Integer, HashMap<String, Integer>> borrowedBook = StudentMember.getBorrowedBook();
                    HashMap<String, Integer> bor = borrowedBook.get(id);
                    for (String key: bor.keySet()){
                        if (key.contains(book.isbn.toString()) == true){
                            bor.remove(key);
                            break;
                        }
                    }
                    borrowedBook.put(id, bor);
                    StudentMember.setBorrowedBook(borrowedBook);

                }else{
                    Map<Integer, HashMap<String, Integer>> staffBorrowedBook = StaffMember.getBorrowedBook();
                    HashMap<String, Integer> bor = staffBorrowedBook.get(id);
                    for (String key: bor.keySet()){
                        if (key.contains(book.isbn.toString()) == true){
                            bor.remove(key);
                            break;
                        }
                    }
                    staffBorrowedBook.put(id, bor);
                    StaffMember.setBorrowedBook(staffBorrowedBook);
                }            
                book.bookId = book.bookId + 1;
                this.books.set(ind, book);
            }
        }
    
}
