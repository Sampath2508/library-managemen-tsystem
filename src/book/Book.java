
package book;

public class Book {
    public Integer isbn, year, bookCopy;
    public String name, author;
    public Integer bookId;

    public Book(Integer isbn, String name, String author, Integer year, Integer bookCopy){
        this.isbn = isbn;
        this.name = name;
        this.author = author;
        this.year = year;
        this.bookCopy = bookCopy;
        this.bookId = bookCopy;
    }

    public Book() {
    }
}
