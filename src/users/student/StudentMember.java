package users.student;
import users.*;
import java.util.*;
import Helper.Util.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.json.simple.parser.ParseException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.*;


public class StudentMember {
    
    private static List<Student> students = new ArrayList<Student>();
    private static Map<Integer, HashMap<String, Integer>> borrowedBook = new HashMap<Integer, HashMap<String, Integer>>();
    private static Map<Integer, String> reservedBook = new HashMap<Integer, String>();

    public StudentMember(){}

    public class CurrentStudent extends Users{
        private Integer stuId;
        private String studentName;

        public String getStudentName(){
            return studentName;
        }
    
        public void setStudentData(String name, Integer id){
            studentName = name;
            stuId = id;
        }
    
        public Integer getStuId(){
            return stuId;
        }

        @Override
        public void generateBorrowedBook(){
            if (borrowedBook.size() == 0) System.out.println("No Borrowed Book Founded..");
            else{
                System.out.println("Students Id : " + this.stuId);
                HashMap<String, Integer> bor = borrowedBook.get(this.stuId);
                for(String key : bor.keySet()){
                    System.out.println(String.format("\nBook Id : %s\nReturn Day : %o", key, bor.get(key)));
                }
            }
        }

        @Override
        public void renewBook(String bookId){
            if (StudentMember.borrowedBook.get(this.stuId) == null){
                System.out.println("Your don't have any book yet.....");
            }else{
                HashMap<String, Integer> stuBor = StudentMember.borrowedBook.get(this.stuId);
                Integer returnDate = stuBor.get(bookId) + renewExtendDay;
                stuBor.put(bookId, returnDate);
                StudentMember.borrowedBook.put(this.stuId, stuBor);
                System.out.println("Return Day for the Book " + returnDate);
        }
      }
    }

    public static List<Student> getStudents(){
        return students;
    }

    public static Map<Integer, HashMap<String, Integer>> getBorrowedBook(){
        return borrowedBook;
    }

    public static void setBorrowedBook(Map<Integer, HashMap<String, Integer>> updatedBorrowedBook){
        borrowedBook = updatedBorrowedBook;
    }

    public void readStu() throws ParseException, ParserConfigurationException, SAXException, IOException, ArrayIndexOutOfBoundsException{
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();

            Document document = db.parse(new File("C:\\Users\\inc1560\\Music\\LibraryManagementSystem\\LibraryManagementSystem\\files\\Student.xml"));

            document.getDocumentElement().normalize();

            NodeList Students = document.getElementsByTagName("student");
            
            for(int i = 0; i < Students.getLength(); i++){
                Node student = Students.item(i);
                if(student.getNodeType() == Node.ELEMENT_NODE){
                    Element detail = (Element) student;
                    addStudent(Input.Int(detail.getAttribute("id")), detail.getElementsByTagName("name").item(0).getTextContent(), detail.getElementsByTagName("password").item(0).getTextContent(), detail.getElementsByTagName("email").item(0).getTextContent(), detail.getElementsByTagName("department").item(0).getTextContent());
                }
            }   
    }

    public static void addStudent(Integer id, String userName, String pass, String email, String department){
        students.add(new Student(id, userName, pass, email, department));
    }
}
