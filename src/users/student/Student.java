package users.student;

public class Student {
    public int id, fineAmount;
    public String userName, password, email, department;
    public Student(int id, String userName, String pass,String email, String department){
        this.userName = userName;
        this.id = id;
        this.password = pass;
        this.department = department;
        this.email = email;
        this.fineAmount = 0;
    }
}
