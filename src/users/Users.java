package users;


public abstract class Users{
   public final Integer renewExtendDay = 5;
   public abstract void renewBook(String bookId);
   public abstract void generateBorrowedBook();
}