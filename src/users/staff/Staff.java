package users.staff;
import users.*;

public class Staff{
    public Integer id;
    public String userName, password, email, department, subject;
    public Staff(Integer id, String userName, String password, String email, String department, String subject){
        this.id = id;
        this.userName = userName;
        this.password = password;
        this.email = email;
        this.department = department;
        this.subject = subject;
    }
}
