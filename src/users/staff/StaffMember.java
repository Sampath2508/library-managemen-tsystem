package users.staff;

import Helper.Util.*;

import users.*;
import java.util.*;
import java.io.*;

public class StaffMember{
    private static List<Staff> staffs = new ArrayList<Staff>();
    private static Map<Integer, HashMap<String, Integer>> staffBorrowedBook = new HashMap<Integer, HashMap<String, Integer>>();
    

    public class CurrentStaff extends Users{
        private String staffName;
        private Integer staffId = -1;

        public String getStaffName(){
            return staffName;
        }
    
        public void setStaffData(String name, Integer id){
            staffName = name;
            staffId = id;
        }
    
        public Integer getStaffId(){
            return staffId;
        }

        public void setStaff(Integer staffId, String staffName){
            this.staffId = staffId;
            this.staffName = staffName;
        }

        @Override
        public void renewBook(String bookId){
            if (StaffMember.staffBorrowedBook.get(this.staffId) == null){
                System.out.println("Your don't have any book yet.....");
            }else{
                HashMap<String, Integer> staffBor = StaffMember.staffBorrowedBook.get(this.staffId);
                Integer returnDate = staffBor.get(bookId) + renewExtendDay;
                staffBor.put(bookId, returnDate);
                StaffMember.staffBorrowedBook.put(this.staffId, staffBor);
                System.out.println("Return Day for the Book " + returnDate);
            }
        }

        @Override
        public void generateBorrowedBook(){
            if (staffBorrowedBook.size() == 0) System.out.println("No Borrowed Book Founded..");
            else{
                System.out.println("Staff Id : " + this.staffId);
                HashMap<String, Integer> bor = staffBorrowedBook.get(this.staffId);
                for(String key : bor.keySet()){
                    System.out.println(String.format("\nBook Id : %s\nReturn Day : %o", key, bor.get(key)));
                }
            }  
        }

    }

    public static List<Staff> getStaffs(){
        return staffs;
    }

    
    
    public void readStaff(){
        try{
            BufferedReader data = new BufferedReader(new FileReader("C:\\Users\\inc1560\\Music\\LibraryManagementSystem\\LibraryManagementSystem\\files\\Staff.csv"));
            for(String line = data.readLine(); line != null; line = data.readLine()){
                String[] al = line.split(",");
                addStaff(al[0], al[1], al[2], al[3], al[4], al[5]);
            }
            data.close();
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }

    public static Map<Integer, HashMap<String, Integer>> getBorrowedBook(){
        return staffBorrowedBook;
    }

    public static void setBorrowedBook(Map<Integer, HashMap<String, Integer>> updatedBorrowedBook){
        staffBorrowedBook = updatedBorrowedBook;
    }

    public static void addStaff(String id, String userName, String password, String email, String department, String subject){
        staffs.add(new Staff(Input.Int(id), userName, password, email, department, subject));
    }
}
