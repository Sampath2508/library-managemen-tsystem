package admin;

public class Librarian{
    public Integer id;
    public String name, password, email;
    public Librarian(Integer id, String name, String password, String email){
        this.id = id;
        this.name = name;
        this.password = password;
        this.email = email;
    }
}