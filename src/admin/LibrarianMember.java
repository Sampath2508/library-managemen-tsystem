package admin;
import Helper.Util.Input;

import java.io.*;

import book.BookStore;

import java.util.*;

import users.staff.StaffMember;
import users.student.StudentMember;

public class LibrarianMember extends BookStore{
    private static int id;
    private static String name;
    private static List<Librarian> librarians = new ArrayList<Librarian>();

    public static List<Librarian> getLibrarians(){
        return librarians;
    }

    public static String getLibUserName(){
        return name;
    }

    public static void setLibData(String name, Integer id){
        name = name;
        id = id;
    }

    public static Integer getStaffId(){
        return id;
    }

    public void readLib(){
        try{
            BufferedReader data = new BufferedReader(new FileReader("C:\\Users\\inc1560\\Music\\LibraryManagementSystem\\LibraryManagementSystem\\files\\Librarian.csv"));
            for(String line = data.readLine(); line != null; line = data.readLine()){
                String[] al = line.split(",");
                addLibrarian(Input.Int(al[0]), al[1], al[2], al[3]);
            }
            data.close();
        }
        catch(IOException e){
            e.printStackTrace();
        }
        catch(ArrayIndexOutOfBoundsException e){
            e.printStackTrace();
        }
    }

    
    public void addLibrarian(Integer id, String userName, String pass, String email){
        this.librarians.add(new Librarian(id, userName, pass, email));
    }

    

   
}